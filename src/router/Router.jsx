import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Header from '../components/Header'
import Slider from '../views/Slider';
import About from '../views/About';
import Services from '../views/Services';
import Portfolio from '../views/Portfolio';
import Contact from '../views/Contact'
import Error from '../views/Error'
import Admin from '../views/Admin'

class Router extends Component{
    render(){
        return(
            <BrowserRouter>
                <div>
                    <Header />
                    <Switch>
                        <Route exact path='/' component={Slider} />
                        <Route exact path='/nosotros' component={About} />
                        <Route exact path='/servicios' component={Services} />
                        <Route exact path='/portafolio' component={Portfolio} />
                        <Route exact path='/contacto' component={Contact} />
                        <Route exact path='/admin' component={Admin}/>
                        <Route component={Error} />      
                    </Switch>
                </div>
                
            </BrowserRouter>
        )
    }
}

export default Router;