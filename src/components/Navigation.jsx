import React, { Component } from 'react';
import Logo from '../assets/img/coded4.png';
import { Link } from 'react-router-dom';
// Materialize
import M from 'materialize-css';
import logo from '../assets/img/logoside.png'



class Navigation extends Component{


    componentDidMount(){
        var elems = document.querySelectorAll('.sidenav');
         M.Sidenav.init(elems);
         /*var menu = document.querySelectorAll('.dropdown-trigger');
         M.Dropdown.init(menu, {
                coverTrigger: false,
                alignment: 'right'
          });*/
     }

    /**
    show-on-large muestra siempre la burger
    sidenav-fixed deja fijo el side y se oculta en sm 
    */

    render(){
        return(
        <div>

            <nav className='container effect'>
                <div className="nav-wrapper brackets">
                    <Link to={'/'} className="brand-logo"><img src={Logo} alt=""/></Link>
                    <a href="#!" data-target="mobile-demo" className="sidenav-trigger"><i className="material-icons">menu</i></a>
                    <ul className="right hide-on-med-and-down ">
                    <li><Link to={'/'}>Inicio</Link></li>
                    <li><Link to={'/nosotros'}>Nosotros</Link></li>
                    <li><Link to={'/servicios'}>Servicios</Link></li>
                    <li><Link to={'/portafolio'}>Portafolio</Link></li>
                    <li><Link to={'/contacto'}>Contacto</Link></li>     
                    </ul>
                    
                </div>

            </nav>

            
                <ul className="sidenav" id="mobile-demo">
                    <li className="logoSide"><a href="#!"><img src={logo} alt=""/></a></li>
                    <div className='divider white'></div> 
                    <li><Link to={'/'} className="sidenav-close">{'< '}Inicio{' />'}</Link></li>
                    <li><Link to={'/nosotros'} className="sidenav-close">{'< '}Nosotros{' />'}</Link></li>
                    <li><Link to={'/servicios'} className="sidenav-close">{'< '}Servicios{' />'}</Link></li>
                    <li><Link to={'/portafolio'} className="sidenav-close">{'< '}Portafolio{' />'}</Link></li>
                    <li><Link to={'/contacto'} className="sidenav-close">{'< '}Contacto{' />'}</Link></li>
                    <div className='divider white'></div> 
                    <li><a>SEGUIMOS</a></li>
                    <div className='iconsSide'>
                        <a className='butnSide facebook' href='#'>
                            <i className='fab fa-facebook'></i>
                        </a>
                        <a className='butnSide twitter' href='#'>
                            <i className='fab fa-twitter'></i>
                        </a>
                    </div>
                </ul>
                
                
        
                
        </div>
          
        
        )
    }
}

export default Navigation;