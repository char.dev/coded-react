import React from 'react';
import Typed from 'react-typed';

function Footer() {
  return (
    <footer>
        <div className="divider"></div>
        <div className="footer-copyright">
            <div className="items-footer container">
                <div className='copy'>© 2019 CODED</div>
                <div className='market'>
                    <p><span className='sp'>[</span></p>  
                        <div className='typed'>
                            <Typed
                                loop typeSpeed={100} backSpeed={50}
                                strings={[" PROGRAMACION"," DISEÑO"," DESARROLLO"]}
                                smartBackspace shuffle={false}
                                backDelay={1} fadeOut={false}
                                fadeOutDelay={100} showCursor cursorChar="_"
                            />    
                        </div>
                    <span className='sp'>]</span>
                </div>
            </div>
        </div>
    </footer>       
  )
}

export default Footer;