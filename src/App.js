import React, { Component } from 'react';
import Footer from './components/Footer'
import Router from './router/Router'


class App extends Component {
  render() {
    return (

      <div className=''>
         <div className=''>
           <Router />
         </div>
         <div className='hide-on-small-only'>
            <Footer />
         </div>
         
      </div>
     

    );
  }
}

export default App;
