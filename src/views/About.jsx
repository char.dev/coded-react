import React, { Component } from 'react'
import LogoAbout from '../assets/img/codedLogoCyan.png'

class About extends Component{
    render(){
        return(
            <div className="container centrado">
                    <div class="row nosotros">
                            <h5 class="titulo">QUIENES SOMOS
                            <div class="divider white"></div>
                            </h5>

                                <div class="col l6 m12 s12 imgabout">
                                    <img src={LogoAbout} 
                                    class="ml65 logo-about responsive-img" alt="" />
                                </div>

                                <div class="col l6 m12 s12">
                                    <div class="box-text">
                                        <div className="divider cyan"></div>
                                        <p class="description white-text center">
                                        SOMOS UN EQUIPO DE PROGRAMADORES QUE NOS ENCANTA LA TECNOLOGIA Y ESTAMOS ENFOCADOS EN EL DISEÑO, 
                                        DESARROLLO E IMPLEMENTACION DE APLICACIONES WEB, CREEMOS QUE CADA CLIENTE ES ÚNICO. <br></br> NO SOLO NOS ENFOCAMOS EN DAR UN TRATO 
                                        PERSONALIZADO, SINO TAMBIEN QUE BUSCAMOS COMPLEMENTARNOS CON SUS IDEAS DESDE CERO, ANALIZAR LA NECESIDAD, 
                                        DISEÑAR Y DESARROLLAR UN PRODUCTO ADECUADO PROPONIENDO SOLUCIONES APLICANDO NUESTRO CONOCIMIENTO. 
                                        </p>
                                        <div className="divider cyan"></div>
                                    </div>
                                </div>
                    
                    </div>
            </div>

        )
    }
}

export default About;