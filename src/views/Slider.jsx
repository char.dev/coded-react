import React, { Component } from 'react'
import s1 from '../assets/img/slider/s1.jpg'
import s2 from '../assets/img/slider/s2.jpg'
import s3 from '../assets/img/slider/s3.jpg'
import s4 from '../assets/img/slider/s4.jpg'
import M from 'materialize-css'

class Slider extends Component{

    componentDidMount(){
        var elems = document.querySelectorAll('.slider');
        M.Slider.init(elems,{
            height: 400,
            interval: 4000,
            indicators: false
        });
    }

    

    render(){
        return(
            <div className='container centrado'>
            <div className="row services ">
                    <h5 class="titulo">BIENVENIDO
                        <div class="divider white"></div>
                    </h5>
                
                    <div className="slider">
                        <ul className="slides">
                            <li>
                                <img src={s1} alt='imagen1'/>
                                <div className="caption right-align">
                                <h3>PROYECTOS DESARROLLADOS DESDE CERO</h3>
                                <h5 className="light grey-text text-lighten-3">Tu web oficial</h5>
                                </div>
                            </li>
                            <li>
                                <img src={s2} alt='' /> 
                                <div className="caption left-align">
                                <h3>DISEÑOS PERSONALIZADOS</h3>
                                <h5 className="light grey-text text-lighten-3">La web que te representa</h5>
                                </div>
                            </li>
                            <li>
                                <img src={s3} alt=''/>
                                <div className="caption center-align black-text">
                                <h3>VISUALES Y FUNCIONALES</h3>
                                <h5 className="light black-text text-lighten-3">Cuidamos la experiencia del usuario</h5>
                                </div>
                            </li>
                            <li>
                                <img src={s4} alt=''/>
                                <div className="caption left-align borde-text">
                                <h3>APLICACIONES RESPONSIVAS</h3>
                                <h5 className="light border-text text-lighten-3">Adaptables a cualquier dispositivo</h5>
                                </div>
                            </li>
                        </ul>    
                    </div>

                </div>
            </div>
        )
    }
}

export default Slider;
