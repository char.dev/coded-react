import React, { Component } from 'react'

class Admin extends Component{

    state={
        login: false,
        user: '',
        password: ''
    }

    login =() =>{
        if(this.state.login){
            return(
            <div className='container centrado'>
                <div className="row">
                <div className="col m3 menulab">este seria el menu lateral</div>
                <div className="col m8 infos"> esta serai la informacion a mostrar</div>
                </div>    
            </div>)
        }else{
            return(
            
            <div className="row">
                <form className="col m2 s-4 offset-s3 offset-m5 login" onSubmit={this.onLogin}>
                    <div className="input-field">
                    <input id="user" name='user' type="text" className="validate" 
                            value={this.state.user}
                            onChange={this.handleInputChange}/>
                    <label for="user">Usuario</label>
                    </div>
                    <div className="input-field">
                    <input id="password" name='password' type="password" className="validate"
                        value={this.state.password}
                        onChange={this.handleInputChange}
                    />
                    <label for="password">Password</label>
                    </div>
                    <input className="waves-effect btn-small" type="submit" value="Entrar"/>
                    
                </form>
            </div>
         
        )}
    }

    handleInputChange = (event) => {
        const { value, name } = event.target;
        this.setState({
          [name]: value
        });
      }

    onLogin = (event) =>{
        event.preventDefault()
        if(this.state.user === 'admin'){
            if(this.state.password === 'admincoded'){
                console.log('logueado correctamente')
                this.setState({ login : true })
            }else{
            console.log('error al loguearse')
            }
        }
    }



    render(){
        
        return(
            this.login()
        )
    }
}

export default Admin;