import React, { Component } from 'react'


class Contact extends Component{
    render(){
        return(
            <div className='container centrado'>
                <div className="row services ">
                        <h5 class="titulo">CONTACTENOS
                            <div class="divider white"></div>
                        </h5>

                        <div class="col l7 m12 s12">
                            <form class="row">
                                <div class="input-field col l6 s8">
                                    <i class="material-icons prefix white-text" >person_circle</i>
                                    <input id="name" type="text" className='validate' />
                                    <label for="name">Nombre</label>
                                </div>

                                <div class="input-field col l8 s10">
                                    <i class="material-icons prefix white-text">email_circle</i>
                                    <input id="email" type="email" className='validate'/>
                                    <label for="email">Email</label>
                                </div>
                                <div class="input-field col l10 s12">
                                    <i class="material-icons prefix white-text">mode_edit</i>
                                    <textarea id="msj" class="materialize-textarea"></textarea>
                                    <label for="msj">Mensaje</label>
                                </div>

                                <div className='col offset-l4 offset-s6'>
                                    <button class="btn blue darken-2 waves-effect waves-light" type="submit" name="action">
                                        Enviar
                                        <i class="material-icons right">send</i>
                                    </button>  
                                </div>
                                
                            </form>
                        </div>

                        <div class="col l5 m12 s12">
                            <div className='row'>
                                <p class="titulo"> NUESTRAS REDES SOCIALES </p>
                                 <div class="divider white"></div>
                                <div className='col l12 offset-s3 s6'>
                                    <div className='icons'>
                                        <a className='butn facebook' href='#'>
                                            <i className='fab fa-facebook'></i>
                                        </a>
                                        <a className='butn twitter' href='#'>
                                            <i className='fab fa-twitter'></i>
                                        </a>
                                        <a className='butn instagram' href='#'>
                                            <i className='fab fa-instagram'></i>
                                        </a>
                                    </div>
                                </div>
                             </div>
                        </div>
           
                </div>
            </div>
        )
    }
}

export default Contact;
