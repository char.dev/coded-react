import React from 'react'
import Design from '../assets/img/services/designWeb.jpg'
import Database from '../assets/img/services/database.jpg'
import Hosting from '../assets/img/services/hosting.jpg'
import Posinamiento from '../assets/img/services/posicionamiento.jpg'
import Mantenimiento from '../assets/img/services/mantenimiento.jpg'
import Segurity from '../assets/img/services/security.jpg'


export default function Services() {
  return (
    <div className='container centrado'>
    <div className="row services">
            <h5 className="titulo">NUESTROS SERVICIOS
            <div className="divider white"></div>
            </h5>
            <div className="cards">
                <div className="col l4 m6 s12">
                    <div className="card hoverable">
                        <div className="card-image waves-effect waves-block waves-light">
                            <img className="activator responsive-img" src={Design} alt="design"/>
                        </div>
                        <div className="card-content">
                            <span className="card-title activator grey-text text-darken-4">Diseño y desarrollo<i className="material-icons right">expand_less</i></span>
                        </div>
                        <div className="card-reveal">
                            <span className="card-title grey-text text-darken-4">Diseño y desarrollo<i className="material-icons right">close</i></span>
                            <p>Nos enfocamos en diseñar, rediseñar y desarrollar aplicaciónes para cualquier plataforma,
                                adaptable a cualquier dispositivo y utilizamos las ultimas tecnologías para blindarles la mejor calidad
                            </p>
                        </div>
                    </div>
                </div>
                <div className="col l4 m6 s12">
                    <div className="card hoverable">
                        <div className="card-image waves-effect waves-block waves-light">
                            <img className="activator responsive-img" src={Hosting} alt="design"/>
                        </div>
                        <div className="card-content">
                            <span className="card-title activator grey-text text-darken-4">Almacenamiento<i className="material-icons right">expand_less</i></span>
                        </div>
                        <div className="card-reveal">
                            <span className="card-title grey-text text-darken-4">Almacenamiento<i className="material-icons right">close</i></span>
                            <p>Porque la información es importante, brindamos los mejores diseños de almacenamiento, para contemplar sus necesidades,
                                además la seguridad de sus datos.
                            </p>
                        </div>
                    </div>
                </div>
                <div className="col l4 m6 s12">
                    <div className="card hoverable">
                        <div className="card-image waves-effect waves-block waves-light">
                            <img className="activator responsive-img" src={Mantenimiento} alt="design"/>
                        </div>
                    <div className="card-content">
                    <span className="card-title activator grey-text text-darken-4">Mantenimiento<i className="material-icons right">expand_less</i></span>
                    </div>
                    <div className="card-reveal">
                    <span className="card-title grey-text text-darken-4">mantenimiento<i className="material-icons right">close</i></span>
                    <p>Te brindamos un servicio completo, personalizado en materia de mantenimiento, actualización de servicios web, para que tu
                        aplicación esté siempre actualizada, adaptándola a funcionalidades que necesite.
                    </p>
                    </div>
                    </div>
                </div>
                <div className="col l4 m6 s12">
                    <div className="card hoverable">
                        <div className="card-image waves-effect waves-block waves-light">
                            <img className="activator responsive-img" src={Database} alt="design"/>
                        </div>
                    <div className="card-content">
                    <span className="card-title activator grey-text text-darken-4">dominio y alojamiento<i className="material-icons right">expand_less</i></span>
                    </div>
                    <div className="card-reveal">
                    <span className="card-title grey-text text-darken-4">Dominio y alojamiento<i className="material-icons right">close</i></span>
                    <p>Registramos tu dominio tanto nacional como internacional, de una manera fácil 
                        y nos encargamos de su registro y renovación, ademas alojamos tu app
                        garantizándote velocidad, tecnología y calidad
                    </p>
                    </div>
                    </div>
                </div>
                <div className="col l4 m6 s12">
                    <div className="card hoverable">
                        <div className="card-image waves-effect waves-block waves-light">
                            <img className="activator responsive-img" src={Segurity} alt="design"/>
                        </div>
                    <div className="card-content">
                    <span className="card-title activator grey-text text-darken-4">Seguridad<i className="material-icons right">expand_less</i></span>
                    </div>
                    <div className="card-reveal">
                    <span className="card-title grey-text text-darken-4">seguridad<i className="material-icons right">close</i></span>
                    <p>El incremento de la información en medios como las app ha generado una gran cantidad 
                        de datos sensibles y como desarrolladores una de nuestras tareas es ser capaces de alcanzar 
                        el mayor nivel de seguridad y protección.
                    </p>
                    </div>
                    </div>
                </div>
                <div className="col l4 m6 s12">
                    <div className="card hoverable">
                        <div className="card-image waves-effect waves-block waves-light">
                            <img className="activator responsive-img" src={Posinamiento} alt="design "/>
                        </div>
                    <div className="card-content">
                    <span className="card-title activator grey-text text-darken-4">Posicionamiento<i className="material-icons right">expand_less</i></span>
                    </div>
                    <div className="card-reveal">
                    <span className="card-title grey-text text-darken-4">posicionamiento<i className="material-icons right">close</i></span>
                    <p>Optimizamos su app tanto en buscadores como al usuario usando
                        técnicas transparentes. La mejor forma de estar bien posicionado es ofrecer una app
                        realizada de forma profesional tanto en diseño como en programación.
                    </p>
                    </div>
                    </div>
                </div>
            </div>
    </div>
</div>
  )
}
